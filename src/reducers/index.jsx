const addGood = (payload, state) => {
    const sameGoods = state.cart.find(item => item.id === payload.good.id);
    if (sameGoods) {
        sameGoods.count += payload.goodCount;
    } else {
        payload.good.count = payload.goodCount;
    }
    const latestsGoods = sameGoods === undefined ? [...state.cart, payload.good] : [...state.cart];
    localStorage.setItem('cart', JSON.stringify(latestsGoods));
    return {
        ...state,
        cart: latestsGoods
    };
};
const delGood = (goodID, state) => {
    const latestsGoods = state.cart;
    latestsGoods.splice(goodID, 1);
    localStorage.setItem('cart', JSON.stringify(latestsGoods));
    return {
        ...state,
        cart: latestsGoods
    };
};
const getCartLocal = (state) => {
    const localCart = localStorage.getItem('cart') !== null ? JSON.parse(localStorage.getItem('cart')) : [];
    return {
        ...state,
        cart: localCart
    };
};
const checkoutOrder = (state) => {
    fetch('/script.php', {
        method: "POST",
        mode: "same-origin",
        credentials: "same-origin",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
            contacts: {
                name: document.forms['checkout-form'].elements['user_name'].value,
                email: document.forms['checkout-form'].elements['user_email'].value,
                phone: document.forms['checkout-form'].elements['user_phone'].value,
            },
            data: state.cart
        })
    })
    .then(() => alert("Заказ принят"));
    return state;
};
const toggleCart = (state) => {
    return {
        ...state,
        cartIsOpen: !state.cartIsOpen
    };
};

export default (state, action) => {
    switch (action.type) {
        case 'GET_ORDER':
            return getCartLocal(state);
        case 'TOGGLE_CART':
            return toggleCart(state);
        case 'ADD_TO_CART':
            return addGood(action.payload, state);
        case 'DELETE_FROM_CART':
            return delGood(action.payload, state);
        case 'CHECKOUT_ORDER':
            return checkoutOrder(state);
        default:
            return state;
    }
}