import React, {useContext, useState} from 'react';
import CountHandler from './CountHandler';

import './index.css';
import CartContext from '../../../context/CartContext'

const Good = props => {
    const context = useContext(CartContext);
    const [countState, setCountState] = useState(1);

    const handlerPlusCount = () => {
        setCountState(countState + 1);
    }
    const handlerMinusCount = () => {
        countState > 1 && setCountState(countState - 1);
    }

    return (
        <div key={props.item.name + props.id} id={props.item.name + props.id} className='good'>
            <div className='good-img-container'>
                <img src={props.item.img} alt='' width='100%' />
            </div>
            <h4>{props.item.name}</h4>
            <p>{props.item.description}</p>
            <p>{props.item.price}$</p>
            <CountHandler plusGood={handlerPlusCount} minusGood={handlerMinusCount} goodCount={countState} />
            <button onClick={() => context.addGood(props.item, countState)} >Add to cart</button>
        </div>
    )
};

export default Good;