import React from 'react';

import './index.css';

const CountHandler = props => {

    return (
        <div className='counter-wrapper'>
            <span className='counter-button' onClick={props.minusGood}>-</span>
            <span className='counter-field'>{props.goodCount}</span>
            <span className='counter-button' onClick={props.plusGood}>+</span>
        </div>
    );
}

export default CountHandler;