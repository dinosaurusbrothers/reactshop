import React from 'react';
import { goodsData } from './data';
import Good from './Good';

import './index.css';

const Goods = () => {
    const data = goodsData.map((item, id) => {
        return (
            <Good
                key={id}
                id={id}
                item={item}
            />
        );
    })

    return (
        <div className='goods-wrapper'>
            {data}
        </div>
    )
}

export default Goods;