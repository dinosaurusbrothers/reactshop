import React, { useContext } from 'react';

import GoodCounter from './GoodCounter';
import './index.css';

import CartContext from '../../context/CartContext';

const CartButton = () => {
    const cartContext = useContext(CartContext);

    const goodCount = cartContext.cart.length > 0 ? cartContext.cart.reduce((sum, current) => {
        return sum += current.count;
    }, 0) : 0;

    return (
        <span className='cart-button' onClick={cartContext.toggleCart}>
            <GoodCounter goods={goodCount} />
        </span>
    );
};

export default CartButton;