import React from 'react';

import './index.css';

const GoodCounter = props => {
    return (
        <span className='good-counter'>{props.goods}</span>
    );
}

export default GoodCounter;