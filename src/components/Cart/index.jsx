import React, {useContext, useState, useEffect} from 'react';

import './index.css';
import CartContext from '../../context/CartContext';

const Cart = () => {
    const cartContext = useContext(CartContext);

    useEffect(() => {
        !CartContext.cartIsOpen && setStateForm({isOpen: false});
    }, [cartContext.cartIsOpen]);

    const [stateForm, setStateForm] = useState({
        isOpen: false
    });
    let commonPrice = 0;

    const goods = cartContext.cart.length > 0 && cartContext.cart.map((good, id) => {
        commonPrice += (good.price * good.count);
        return (
            <article key={good.name + id} className='good-in-cart'>
                {good.name} - {good.price}$
                <span type='text' className='good-count'>Кол-во: {good.count}</span>
                <span className='good-delete-btn' onClick={() => cartContext.delGood(id)}>delete</span>
            </article>
        )
    });

    const handlerCheckoutForm = () => {
        setStateForm({
            isOpen: !stateForm.isOpen
        })
    };

    return (
        <div className={`cart-shadow ${!cartContext.cartIsOpen && 'hidden'}`}>
            <div className={`bucket ${!cartContext.cartIsOpen && 'hidden'}`} id="target" >
                <span className='bucket-close-btn' onClick={cartContext.toggleCart}>close</span>
                <div className='cart-goods-list'>
                    {cartContext.cart.length > 0 ? goods : 'Drop there goods'}
                </div>
                <p>Price: {commonPrice}$</p>
                <form id='checkout-form' action="javascript:void(null);" className={`checkout_form ${stateForm.isOpen ? 'is_open' : null}`} onSubmit={cartContext.checkoutOrder}>
                    <input type="text" name="user_name" id="user_name" placeholder='Your Name' />
                    <input type="email" name="user_email" id="user_email" placeholder='E-mail' />
                    <input type="phone" name="user_phone" id="user_phone" placeholder='Phone' />
                    <input type='submit' value='КУПИТЬ' />
                </form>
                <button onClick={handlerCheckoutForm}>{stateForm.isOpen ? 'ОТМЕНА' : 'ОФОРМИТЬ ЗАКАЗ'}</button>
            </div>
        </div>
    )
}

export default Cart;