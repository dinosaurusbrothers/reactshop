import React, {useContext, useReducer, useEffect} from 'react';
import CartContext from './CartContext';
import ShoprReducer from '../reducers';

const GlobalContext = props => {
    const Context = useContext(CartContext);
    const [state, dispatch] = useReducer(ShoprReducer, Context);

    useEffect(() => {
        dispatch({type:'GET_ORDER'});
    }, []);

    const addGood = (good, goodCount) => {
        dispatch({type:'ADD_TO_CART', payload: {good, goodCount}});
    };

    const delGood = (good) => {
        dispatch({type:'DELETE_FROM_CART', payload: good});
    };

    const checkoutOrder = () => {
        dispatch({type:'CHECKOUT_ORDER'});
    };

    const toggleCart = () => {
        dispatch({type:'TOGGLE_CART'});
    };

    return (
        <CartContext.Provider value={{
            cart: state.cart,
            cartIsOpen: state.cartIsOpen,
            addGood,
            delGood,
            checkoutOrder,
            toggleCart
        }}>
            {props.children}
        </CartContext.Provider>
    );
};

export default GlobalContext;