import React from 'react';

const CartContext = React.createContext({
    cart:[],
    cartIsOpen: false
});

export default CartContext;
