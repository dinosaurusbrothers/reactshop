import React from 'react';
import './App.css';

import GlobalContext from './context/GlobalContext';
import Cart from './components/Cart';
import Goods from './components/Goods';
import CartButton from './components/CartButton';

const App = () => {
  return (
    <GlobalContext>
      <h1>ReactShop</h1>
      <div className="App">
        <Cart />
        <Goods />
        <CartButton />
      </div>
    </GlobalContext>
  );
}

export default App;
